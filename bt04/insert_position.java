Node InsertNth(Node head, int data, int position) {
   Node node = new Node();
   node.data = data;
   Node current = head;
   int i = 0;
   if(head == null) return node;
   if(position ==0){
       node.next = head;
       return node;
   }
   while(i!=position-1){
      current = current.next;
      i++;
   }
   node.next = current.next;
   current.next = node;
   return head;
}